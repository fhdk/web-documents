---
title: 'Check if system needs reboot'
date: '06:23 20-11-2022'
taxonomy:
    category:
        - docs
---

**Difficulty: ★★☆☆☆**
## Description
Some system updates **will** require you to restart your system. Packages includes microcode, kernel, driver, xorg and systemd as likely candidates. Below is a list of packages which may warrent a system restart. If you can think of other packages to include in the check just add the package(s) to the **$reboot** regex in the script.

* ucode
* cryptsetup
* linux
* nvidia
* mesa
* systemd
* wayland
* xf86-video
* xorg

## Utility script
The script calls the `checkupdates` script - and `yay` if installed - and uses regular expression to suggest a possible system restart.

```
#!/usr/bin/env bash
reboot="(ucode|cryptsetup|linux|nvidia|mesa|systemd|wayland|xf86-video|xorg)"
if [[ $(which yay) =~ (yay) ]]; then
    updates=$(checkupdates; yay -Qua)
else
    updates=$(checkupdates)
fi
echo "$updates"
if [[ $updates =~ $reboot ]]; then
    echo "Updating possibly requires system restart ..."
fi
```

## Usage
As noted in comments the check is very broad and not intended by any means to be authoritative - it only serves the purpose of suggesting - before updates are installed - if system restart could be necessary.

## Credit and inspiration
Inspiration from @Kresimir and EndeavourOS forum [[1]] [[2]] and the resulting script [[3]]

Posted at [Manjaro Form][4]

[1]: https://forum.endeavouros.com/t/check-if-a-reboot-is-neccessary/7092/37
[2]: https://forum.endeavouros.com/t/random-question-how-often-do-you-update-your-packages/7317/13
[3]: https://forum.endeavouros.com/t/random-question-how-often-do-you-update-your-packages/7317/40
[4]: https://forum.manjaro.org/t/root-tip-utility-script-check-if-updates-may-require-system-restart/14112
