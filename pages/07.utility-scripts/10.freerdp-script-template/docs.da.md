---
title: 'FreeRDP script skabelon'
taxonomy:
    category:
        - docs
published: true
---

For nogen tid siden fik jeg problemer med krdc - remote desktop app - og jeg blev så irriteret at jeg lavede en skabelon til brug for mine RDP forbindelser og på den måde begrænsede min afhængighed til **freerdp**

Skabelonen finder selv ud af om den aktive session er wayland eller x11

Ligesom al anden kode er skabelonens indhold på engelsk.
```
#!/usr/bin/env bash
#
# Script template to open FreeRDP connection
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# root.nix.dk
# - 2022-11-23 : moved width and height to variables section
# - 2023-03-23 : fixed missing $ at variable in xfreerdp call
#              : added comments to customize section
# - 2024-06-05 : added dependency on 'freerdp2' package
#
#######################################################
### begin customize
# Documentation
# https://github.com/FreeRDP/FreeRDP/wiki/CommandLineInterface
#
# to list available keyboards run 'xfreerdp /kbd-list'
# examples
# french => 0x0000040C
# german => 0x00000407
# us     => 0x00000409
# uk     => 0x00000809
# danish => 0x00000406

# set keyboard 
KEYBOARD="0x00000406"

# set screen size
WIDTH=1920
HEIGHT=1080

# define host and credentials to be used
REMOTE=""             # hostname or ip-address
USERNAME=""           # login
PASSWORD=""           # passwd

# connnection type - you may need to adjust if you have a slow or poor connection
NETWORK="lan"         # connection type
PORT="3389"           # rdp default

# share - if you don't need then comment the line reading
#             /drive:${SHARENAME},${SHAREPATH} \
SHAREPATH="/home/$USER"      # shared folder
SHARENAME="media"            # share name on remote system

### end customize
#######################################################
# Recent changes with freerdp has split the binaries into package **freerdp2**
if ! [[ $(pacman -Qoq wlfreerdp) =~ "freerdp2" ]]; then
    echo "package 'freerdp2' is not synced"
    echo "this script requires the binaries 'wlfreerdp' and 'xfreerdp'"
    echo "please sync the package using your package manager"
    exit 1
fi


if [[ ${XDG_SESSION_TYPE} = "wayland" ]]; then
        /usr/bin/wlfreerdp /bpp:24 /audio-mode:2 /sound:sys:alsa /rfx /cert-ignore +clipboard \
        -decorations /parent-window:31 \
        /w:${REMOTEW} /h:${REMOTEH} \
        /kbd:${KEYBOARD} \
        /network:${NETWORK} \
        /drive:${SHARE},${SHAREPATH} \
        /u:${USERNAME} /port:${PORT} /v:${REMOTE} /p:${PASSWORD} &
else
        /usr/bin/xfreerdp /bpp:24 /audio-mode:2 /sound:sys:alsa /rfx /cert-ignore  +clipboard \
        /w:${REMOTEW} /h:${REMOTEH} \
        /kbd:${KEYBOARD} \
        /network:${NETWORK} \
        /drive:${SHARE},${SHAREPATH} \
        /u:${USERNAME} /port:${PORT} /v:${REMOTE} /p:${PASSWORD} &
fi
```

## Anvendelse
Forvis dig om at du har mappen **~/.local/bin** - hvis ikke oprettes den

    mkdir -p ~/.local/bin

Opret en ny file i mappen og indsæt indholdet fra skabelonen i den nye fil.

Gem filen med en navn der giver mening for dig - f.eks. dit **kaldenavn** for den maskine du forbinder til. En bemærkning hertil er - undgå danske tegn som æøå - på sigt er du bedre stillet.

I filen skal du justere egenskaberne i toppen af scriptet - lige under hovedet - så det passer til dine omstændigheder - og filen gemmes.

Marker file som eksekverbar

    chmod +x ~/.local/bin/kaldenavn

Herefter kan du åbne forbindelse fra en terminal ved at skrive filnavnet og trykke **Enter**

    kaldenavn

Hvis du gerne vil have en genvej i dit menu system eller på skrivebordet (afhængig af miljø) skal du oprette en genvejsfil.

Opret genvejen i mappen **~/.local/share/applications/kaldenavn.desktop** med nedenstående indhold

```
[Desktop Entry]
Name=kaldenavn RDP
Icon=network-server
Comment=Opret RDP forbindelse til kaldenavn
Exec="~/.local/bin/kaldenavn"
Version=1.0
Type=Application
Terminal=false
StartupNotify=true
```

Også posted følgende fora

* [Manjaro Forum](https://forum.manjaro.org/t/root-tip-utility-script-freerdp-script-template/125680)
* [EndeavourOS Forum](https://forum.endeavouros.com/t/remmina-with-gnome-43/33390/4)
