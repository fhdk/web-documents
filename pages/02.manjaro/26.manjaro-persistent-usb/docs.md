---
published: true
date: '2019-10-01 00:00'
publish_date: '2019-10-01 00:00'
title: 'Manjaro Persistent USB'
taxonomy:
    category:
        - docs
metadata:
    author: linux-aarhus
---

### Based on the ALMA project

[https://forum.manjaro.org/t/manjaro-xfce-20-0-3-persistent-usb-released/151622][1]

### Presets

[https://github.com/philmmanjaro/alma/tree/master/presets][2]


[1]: https://forum.manjaro.org/t/manjaro-xfce-20-0-3-persistent-usb-released/151622
[2]: https://github.com/philmmanjaro/alma/tree/master/presets
