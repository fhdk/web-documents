---
title: 'AUR - please restrain yourself'
date: '09:57 02-11-2022'
taxonomy:
    category:
        - docs
---

linux-aarhus | 2022-02-20 12:17:20 UTC | Source: [AUR - please restrain yourself](https://forum.manjaro.org/t/aur-please-restrain-yourself/103318)

## Responsible use of AUR
With reference to an earlier thread - drifting out of hand

* https://forum.manjaro.org/t/responsible-use-of-aur/86392

It appears that Manjaro users are still seeing AUR as something to be used without reflection.

The users of Manjaro has become a joke when it comes to AUR

* https://forum.endeavouros.com/t/is-aur-down-again/24287/3

![image|534x155](upload://7kZeOqrxivhys85BqXj1QdgzJA8.png)

Topics like below emphasizes that members of this forum does not realize that AUR scripts are unsupported.

* https://forum.manjaro.org/t/pamac-showing-aur-updates-but-wont-update/78775
* https://forum.manjaro.org/t/aur-updates-does-not-show-up-in-pamac/44363
* https://forum.manjaro.org/t/does-updating-system-with-pacman-updates-aur-packages-as-well/95282
* https://forum.manjaro.org/t/pamac-doesnt-recognise-updates/102763/
* https://forum.manjaro.org/t/pamac-crashes-when-checking-dependencies/102957
* https://forum.manjaro.org/t/some-packages-that-i-cant-update/102867/

While **AUR is** a nice collection of scripts you shouldn't depend on using __unsupported software__.

> **DISCLAIMER: AUR packages are user produced content. Any use of the provided files is at your own risk.** - https://aur.archlinux.org/


> **Use the AUR at your own risk!**
> **No support** will be provided by the Manjaro team for any issues that may arise relating to software installations from the AUR. When Manjaro is updated, AUR packages might stop working. **This is not a Manjaro issue** - https://wiki.manjaro.org/index.php/Arch_User_Repository

----

> Below quote is a topic on Arch forum which dates back **2012-10-27**
> 
> > From time to time the AUR, like any website, goes down.
> > 
> > When that happens, there is a simple, three point checklist you can follow:
> > 
> > 1. Check that it is not just a local issue with [downforeveryoneorjustme](http://www.downforeveryoneorjustme.com/). If it is just you, then troubleshoot your machine/network/typing skills. If it appears to be global, proceed to point two.
> > 
> > 2. Do **not**, under any circumstances, open a thread here. If AUR (or any Arch site is down), rest assured someone is working hard to bring it up. If you have reason to believe that you may have discovered this outage, check on IRC or the ML to see if it has been mentioned. At this point, remain calm and do not panic; proceed in an orderly fashion to point three.
> > 
> > 3. If you *must* have access to unsupported software, you can always take the time to learn how to write a [PKGBUILD](https://wiki.archlinux.org/index.php/Pkgbuild) to free yourself from the tyranny of reliance on the AUR. Then, when it does come back up, you can contribute your own which, rest assured, will in due course be unavailable to some pooor soul next time the AUR is down. This is the wheel of life... - https://bbs.archlinux.org/viewtopic.php?id=151536

