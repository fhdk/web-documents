---
title: 'Tools Overview'
taxonomy:
    category:
        - docs
---

## packages
| Tool          | Download          |
|   ---           |       ---                |
| ddrescue  |  pacman -S ddrescue | 
| safecopy  |  pacman -S safecopy  |
| testdisk    |  pacman -S testdisk    |
| photorec  |  pacman -S photorec  |
| foremost  |  pacman -S foremost  |
| sleuthkit   |  pacman -S sleuthkit  |
| r-linux       |  pacman -S r-linux       | 
| mondo      |  pacman -S mondo     |

## bootable systems
| system | download |
|  ---- | --- |
| redo rescue (debian based ISO) | http://redorescue.com/ |
| sysrescd (arch based ISO) | https://www.system-rescue.org/ |
| knoppix (ISO) | http://knoppix.net/  |
| trk3 (ISO) | https://sourceforge.net/projects/archiveos/files/t/trk/trinity-rescue-kit.3.4-build-397.iso/download |     
| rescatux (ISO) | https://www.supergrubdisk.org/rescatux/

## windows bootable
| system | download |
|  ---- | --- |
| Hirens Boot CD | https://www.hirensbootcd.org/files/HBCD_PE_x64.iso |
| Hirens Boot CD Legacy | https://www.hirensbootcd.org/files/Hirens.BootCD.15.2.zip |