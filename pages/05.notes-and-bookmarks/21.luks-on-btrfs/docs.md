---
published: true
date: '2019-10-01 00:00'
publish_date: '2019-10-01 00:00'
title: 'Luks on btrfs'
taxonomy:
    category:
        - docs
metadata:
    author: linux-aarhus
---

Excellent guide on [EndeavourOS forum][1]

[1]: https://forum.endeavouros.com/t/howto-gpt-uefi-install-with-full-disk-encryption-btrfsonluks-with-separate-root-home-and-pkg-subvolumes-hibernation-with-a-swapfile-auto-snapshots-with-easy-system-rollback-gui-boot-into-snapshots/3782
