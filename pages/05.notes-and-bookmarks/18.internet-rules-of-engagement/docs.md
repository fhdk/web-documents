---
published: true
date: '2019-10-01 00:00'
publish_date: '2019-10-01 00:00'
title: 'Internet rules of engagement'
taxonomy:
    category:
        - docs
metadata:
    author: linux-aarhus
---

## Basic rules of engagement

To avoid being tracked - even if you are sneaking through the forest or the neighborhood - you leave traces - so in the end it is a matter of habits.

Here is 5 basic rules of engagement when dealing with the internet.

1. Never browse sites with questionable content (warez, torrents, pornography etc.)
2. Different browsers for different tasks
   * Firefox 
   * Chromium
   * Vivaldi
3. Use alternative search engines
   * duckduckgo
   * startpage
4. Use only one good privacy plugin
   * duckduckgo
   * ublockorigin
   * adblockplus
5. Use a serious VPN provider for anthing that must be kept confidential
   * mullvad
   * protonvpn
   * nordvpn
