---
title: 'Notes on Flatpak maintenance'
date: '08:25 09-05-2023'
taxonomy:
    category:
        - docs
---

[https://man.archlinux.org/man/flatpak.1](https://man.archlinux.org/man/flatpak.1)

```
sudo pacman -Syu
```
```
flatpak update
```
```
flatpak repair app-name
```
```
flatpak reinstall app-name
```
```
flatpak uninstall --all
```
```
flatpak uninstall --unused
```
```
flatpak update --appstream && flatpak update -y
```
```
flatpak remote-info --force --no-gpg-verify flathub
```
```
flatpak info --show-log <app-name>
```
