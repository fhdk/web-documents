---
title: 'Synology 1010 DSM5.2 to DSM6.x'
date: '11:58 24-12-2024'
taxonomy:
    category:
        - docs
---

Source [Upgrade Synology DS1010+ to DSM 6 - kurtweiske.com](https://kurtweiske.com/upgrade-synology-ds1010-to-dsm-6/)

Upgrade Synology DS1010+ to DSM 6

The DS1010+ is almost identical to the DS1511+, but for some reason the DS1010+ isn’t supported under DSM6. This upgrade trick works well, the only side-effecrt is a swapping of the LAN1 and LAN2 LEDs. There is one absolute clenching moment, however.

1. Install latest DSM on DS1010+ (5.2)
2. Enable SSH
3. SSH to system as root (your user likely will not work; DSM 5.x had weird problems with SSH), 
4. edit the following file using vi
   ```
   vi /etc.defaults/synoinfo.conf
   ```
   Change unique=”synology_x86_1010+” to unique=”synology_x86_1511+”  
   Save the file
5. make a copy of `/dev/synoboot.img` 
   ```
   dd if=/dev/synoboot of=/volume1/synoboot.img
   ```
6. Temporarily mount the USB boot device
   ```
   mkdir /mnt/boot
   cd /dev
   mount /dev/synoboot1
   ```
   Edit the grub configuration stored in `boot/grub/grub.conf`
   ```
   vi /mnt/boot/boot/grub/grub.conf
   ```
   Edit the file to look like this
   ```
   serial –unit=0 –speed=115200
   terminal serial
   default 1
   timeout 3
   hiddenmenu
   fallback 0
   
   title SYNOLOGY_1
       root (hd0,0)
       hw_model=DS1511+
       kernel /zImage root=/dev/ram0 ihd_num=0 netif_num=1 syno_hw_version=DS1511+
       initrd /rd.gz
   title SYNOLOGY_2
       root (hd0,1)
       #cksum /grub_cksum.syno
       vender /vender show
       #hw_model
       kernel /zImage root=/dev/md0 ihd_num=0 netif_num=1 syno_hw_version=DS1511+
       initrd /rd.gz
   ```
   Save the file
7. Reboot the diskstation to pick up the new model number
   ```
   reboot
   ```
8. Manually upgrade to 6.0.3-8754
9. Auto-upgrade to 6.2.4-25556
10. Update Modules
11. Update Synology Trust Level (updates certificates for package management)
12. Synology HDD/SSD Offline Update Pack (updates internal drive compatibility list)

