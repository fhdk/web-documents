---
published: false
date: '2019-10-01 00:00'
publish_date: '2019-10-01 00:00'
title: 'The weather icon reference'
taxonomy:
    category:
        - docs
---

The file requires the package **ttf-nerd-fonts-symbols** available from Archlinux community repository. 

This file is named **cx-icon.json** and is used by the script to generate the necessary input for Conky to use.

```
$ cat cx-icon.json
[
  {
    "id": "200",
    "ttf": ""
  },
  {
    "id": "201",
    "ttf": ""
  },
  {
    "id": "202",
    "ttf": ""
  },
  {
    "id": "210",
    "ttf": ""
  },
  {
    "id": "211",
    "ttf": ""
  },
  {
    "id": "212",
    "ttf": ""
  },
  {
    "id": "221",
    "ttf": ""
  },
  {
    "id": "230",
    "ttf": ""
  },
  {
    "id": "231",
    "ttf": ""
  },
  {
    "id": "232",
    "ttf": ""
  },
  {
    "id": "300",
    "ttf": ""
  },
  {
    "id": "301",

    "ttf": ""
  },
  {
    "id": "302",
    "ttf": ""
  },
  {
    "id": "310",
    "ttf": ""
  },
  {
    "id": "311",
    "ttf": ""
  },
  {
    "id": "312",
    "ttf": ""
  },
  {
    "id": "313",
    "ttf": ""
  },
  {
    "id": "314",
    "ttf": ""
  },
  {
    "id": "321",
    "ttf": ""
  },
  {
    "id": "500",
    "ttf": ""
  },
  {
    "id": "501",
    "ttf": ""
  },
  {
    "id": "502",
    "ttf": ""
  },
  {
    "id": "503",
    "ttf": ""
  },
  {
    "id": "504",
    "ttf": ""
  },
  {
    "id": "511",
    "ttf": ""
  },
  {
    "id": "520",
    "ttf": ""
  },
  {
    "id": "521",
    "ttf": ""
  },
  {
    "id": "522",
    "ttf": ""
  },
  {
    "id": "531",
    "ttf": ""
  },
  {
    "id": "600",
    "ttf": ""
  },
  {
    "id": "601",
    "ttf": ""
  },
  {
    "id": "602",
    "ttf": ""
  },
  {
    "id": "611",
    "ttf": ""
  },
  {
    "id": "612",
    "ttf": ""
  },
  {
    "id": "613",
    "ttf": ""
  },
  {
    "id": "615",
    "ttf": ""
  },
  {
    "id": "616",
    "ttf": ""
  },
  {
    "id": "620",
    "ttf": ""
  },
  {
    "id": "621",
    "ttf": ""
  },
  {
    "id": "622",
    "ttf": ""
  },
  {
    "id": "701",
    "ttf": ""
  },
  {
    "id": "711",
    "ttf": ""
  },
  {
    "id": "721",
    "ttf": ""
  },
  {
    "id": "731",
    "ttf": ""
  },
  {
    "id": "741",
    "ttf": ""
  },
  {
    "id": "751",
    "ttf": ""
  },
  {
    "id": "761",
    "ttf": ""
  },
  {
    "id": "762",
    "ttf": ""
  },
  {
    "id": "771",
    "ttf": ""
  },
  {
    "id": "781",
    "ttf": ""
  },
  {
    "id": "800",
    "ttf": ""
  },
  {
    "id": "801",
    "ttf": ""
  },
  {
    "id": "802",
    "ttf": ""
  },
  {
    "id": "803",
    "ttf": ""
  },
  {
    "id": "804",
    "ttf": ""
  }
]
```
