---
published: false
date: '2019-10-01 00:00'
publish_date: '2019-10-01 00:00'
title: 'The python script'
taxonomy:
    category:
        - docs
---

This a python script used to connect to openweatherapi and generate the data for conky to display

```
#!/usr/bin/env python

import argparse
import json
import os
import pprint
import requests
import re
import sys

# From https://openweathermap.org
#   We recommend making calls to the API
#    no more than one time every 10 minutes for one location
#    (city / coordinates / zip-code).
# This is due to the fact that weather data in our system is updated
#   no more than one time every 10 minutes.

CX_ICON = "cx-icon.json"
CURR_WEATHER = "cw"
CWD = os.getcwd()


def repl_func(m):
    """process regular expression match groups for word upper-casing problem"""
    return m.group(1) + m.group(2).upper()


def main(argv):
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument("-a", "--appid",
                        required=True, type=str,
                        help="get yours at https://openweathermap.org")
    parser.add_argument("-p", "--postcode",
                        required=True, type=str,
                        help="the area postcode")
    parser.add_argument("-c", "--country",
                        required=True, type=str,
                        help="the country")
    parser.add_argument("-l", "--lang",
                        required=True, type=str,
                        help="weather text language\n"
                             "=> valid language see https://openweathermap.org/api/one-call-api#multi")
    parser.add_argument("-u", "--units",
                        choices=["imperial", "metric"], type=str,
                        help="display units")

    args = parser.parse_args()
    appid = args.appid
    postcode = args.postcode
    country = args.country
    lang = args.lang
    units = args.units
    response = requests.Response()
    pp = pprint.PrettyPrinter(indent=2)
    try:
        url = f"https://api.openweathermap.org/data/2.5/weather?" \
              f"lang={lang}&" \
              f"units={units}&" \
              f"appid={appid}&" \
              f"zip={postcode},{country}"
        response = requests.get(url).json()
        if response["cod"] > 400 < 500:
            pp.pprint(response)
            exit()
    except Exception as e:
        print(pp.pprint(e))
        exit()

    try:
        with open(CX_ICON, "r") as icon_def:
            icon_list = json.loads(icon_def.read())
    except FileNotFoundError:
        pp.pprint(f"Weather icon file not found: {CWD}/{CX_ICON}")

    w_icon = [icon for icon in icon_list if icon["id"] == str(response["weather"][0]["id"])]

    with open(CURR_WEATHER, "w") as outfile:
        outfile.write(f"{round(float(response['main']['temp']))}\n")
        description = re.sub(r"(^|\s)(\S)", repl_func, response['weather'][0]['description'])
        outfile.write(f"{description}\n")
        if len(w_icon) > 0:
            outfile.write(f"{w_icon[0]['ttf']}\n")
        else:
            outfile.write("\n")


if __name__ == "__main__":
    main(sys.argv[1:])
```