---
title: 'Dummy notes for BIOS/GPT install'
taxonomy:
    category:
        - docs
---

## BIOS/GPT
1. partition
   -  32MB unformatted partition - type EF02
   -  root
2. format root
3. mount root
4. 