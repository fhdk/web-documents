---
title: 'Bypass root permissions'
taxonomy:
    category:
        - docs
---

Original post [@FadeMind][1] Manjaro Forum

**Difficulty: ★★★★☆**

!! **WARNING!** 
!! ONLY apply the configuration after careful considerations of the possible implications.
!!
!! **DISCLAIMER:**
!! BEWARE OF THE UNSPEAKABLE HORRORS WHICH MAY INFEST YOUR SYSTEM AS YOU HAVE **ONLY** YOURSELF TO BLAME. ANY DAMAGES TO YOUR SYSTEM - DIRECTLY OR INDIRECTLY INCLUDING BUT NOT LIMITED TO - MALWARE INFECTION - ACCIDENTAL REMOVAL OF SYSTEM FILES OR SYSTEM MALFUNCTION IS YOUR RESPONSIBILITY.

## Issue
The `sudo` prompt (GUI and terminal) require you to enter the root password each time.

## Main bypass guide

### Step 1
Set  `nano`  temporary as default editor for  `visudo`  and edit sudoers file:
```bash
# sudo -i
# export EDITOR=nano
# visudo
```

### Step 2
To set `nano` as `visudo` default editor. Create the file
```bash
$ sudo nano /etc/sudoers.d/02-visudo-editor
```
with content
```text
## Set nano as default editor
Defaults env_reset
Defaults editor=/usr/bin/nano, !env_editor
```

### Step 3
Skip password prompt for current user.  Create the file
```text
$ sudo nano /etc/sudoers.d/01-skip_auth
```
Replace  `user_name` with your own username
```text
## Skip password prompt for current user
Defaults:user_name !authenticate
```
Save the changes. 
   
**Changes are applied immediately.**

### Step 4
Create a new Polkit rule as root in `/etc/polkit-1/rules.d/49-nopasswd_global.rules` add/change content
```text
/* Allow members of the wheel group to execute any actions
 * without password authentication, similar to "sudo NOPASSWD:"
 */
polkit.addRule(function(action, subject) {
    if (subject.isInGroup("wheel")) {
        return polkit.Result.YES;
    }
});
```
### Step 5
Ensure that you are a member of the `wheel` group:
```
$groups
```
If not add yourself
```
sudo gpasswd -a $USER whell
```
To apply group change logout and login.

## KDE additional bypass guide 
! This section is intended exclusively for KDE users.
Apply the above guide before this section

### Step A
Set `sudo` as the default tool for  **kdesu**  command. Create file  `~/.config/kdesurc` with content:
```
[super-user-command]
super-user-command=sudo
```

### Step B
Add to  `/etc/environment`  these lines:
```
KDE_FULL_SESSION=true
KDE_SESSION_VERSION=5
```

Reboot to apply these settings

[1]: https://forum.manjaro.org/t/howto-bypass-prompt-root-permissions-password-everywhere-20201019/32767