---
title: 'Install LAMP on EndeavourOS (Arch based distribution)'
date: '12:54 20-11-2022'
taxonomy:
    category:
        - docs
---

**LAMP**  is the the acronym of Linux, Apache, MySQL/MariaDB, PHP/Perl/Pyhton.

Installation of
* Apache
* PHP
* MariaDB

The LAMP stack is often extended with a database administration tool **phpMyAdmin**

* phpMyAdmin (webapp to maintain your MariaDB installation)

## Important
Never add packages without a full system sync - to sync your system execute
```text
sudo pacman -Syyu
```
All commands are execute in root context - using 
```
su -l root
```

## Editing system files
Edit system files using a terminal text editory. **micro** is a personal preference just use the one you prefer..

* To save files in **micro** editor press **Ctrl**+**s**
* To exit the **micro** editor press **Ctrl**+**q**
* To search in **micro** editor press  **Ctrl**+**f**

Either install **micro** or substitute micro for your terminal editor of choice
```text
pacman -S micro
```

## Install Apache

Install Apache web server using command
```text
pacman -S apache
```

Edit the `/etc/httpd/conf/httpd.conf` file,
```text
micro /etc/httpd/conf/httpd.conf
```

Search for and comment the following line if it is not already
```text
[...]
# LoadModule unique_id_module modules/mod_unique_id.so
[...]
```

Search for **ServerAdmin** and replace with a valid email
```text
[...]
ServerAdmin you@example.com
[...]
```

Next edit the **ServerName** at the very least enter your system's IP address
```text
[...]
ServerName ip.x.y.z:80
[...]
```
Save and close the file. 

Enable and start the web service
```text
systemctl enable --now httpd
```

Verify the status of the service
```text
# systemctl status httpd
● httpd.service - Apache Web Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; preset: disabled)
     Active: active (running) since Fri 2022-11-11 13:03:33 CET; 4s ago
[...]
Nov 11 13:03:33 test systemd[1]: Started Apache Web Server.
```

## Verify web service
Test the webservice by creating a sample page in the default web root in `/srv/http`
```text
micro /srv/http/index.html
```

Add text - no need for our test to be strictly html compliant
```text
<h2>It works!</h2>
```

Now, open your web browser and navigate to
```text
http://ip.x.y.z
```

You should be greeted with the **It works** message.

## Install MariaDB 

MariaDB is the default implementation of MySQL on Arch based distributions. To install MariaDB execute
```text
pacman -S mariadb
```

Iniitialize the MariaDB data directory prior to starting the service, by using the installer script (do not change --datadir)
```text
mariadb_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
```

When the script has completed enable and start the seervice
```text
systemctl enable --now mariadb
```

You can verify the MariaDB service status (shortened)
```text
# systemctl status mariadb
● mariadb.service - MariaDB 10.9.3 database server
     Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; preset: disabled)
     Active: active (running) since Fri 2022-11-11 13:09:51 CET; 10s ago
[...]
Nov 11 13:09:51 test systemd[1]: Started MariaDB 10.9.3 database server.
```

## Secure your MariaDB service

Don't confuse the root password with your your system password. The root password asked using the script is for MariaDB.
```text
mariadb_secure_installation
```

## Install PHP

Arch always uses latest php version. To install PHP and the apache PHP module
```text
pacman -S php php-apache
```

Proceed to configure Apache PHP module by editing the file `/etc/httpd/conf/httpd.conf`
```text
micro /etc/httpd/conf/httpd.conf
```

Search and locate the following and edit to read as below
```text
[...]
#LoadModule mpm_event_module modules/mod_mpm_event.so
LoadModule mpm_prefork_module modules/mod_mpm_prefork.so
[...]
```

Scroll to the bottom of the file and add for current PHP
```text
LoadModule php_module modules/libphp.so
AddHandler php-script php
Include conf/extra/php_module.conf
```

Save the file and restart the `httpd` service.
```text
systemctl restart httpd
```

## Test PHP

Create a file  `phpinfo.php`  file in the web service root folder
```text
micro /srv/http/phpinfo.php
```

With content
```text
<?php
phpinfo();
```
Save the file and open your web browser and navigate to  `http://ip.x.y.z/phpinfo.php`  which should then provide you with the currennt php configuraiton, enabled modules etc.

## Install phpMyAdmin
phpMyAdmin is a graphical MySQL/MariaDB administration tool that can be used to maintain your MariaDB installation. To install phpMyAdmin
```text
pacman -S phpmyadmin
```

Edit the file  `/etc/php/php.ini` 

```text
micro /etc/php/php.ini
```

Modify the configuration to include the following modules (remove the **;** at the beginning of the line
```text
[...]
extension=bz2
[...]
extension=mysqli
[...]
```
Save and close `php.ini`.

## Create phpMyAdmin configuration
Then create a new  Apache configuration to be able to load phpMyAdmin
```text
micro /etc/httpd/conf/extra/phpmyadmin.conf
```

With content
```text
Alias /phpmyadmin "/usr/share/webapps/phpMyAdmin"
<Directory "/usr/share/webapps/phpMyAdmin">
    DirectoryIndex index.php
    AllowOverride All
    Options FollowSymlinks
    Require all granted
</Directory>
```

Edit the Apache configuraton
```text
micro /etc/httpd/conf/httpd.conf
```

Include phpMyAdmin configuration at the very end of the file
```text
[...]
Include conf/extra/phpmyadmin.conf
```

Save and close the file.

Edit the phpMyAdmin `/etc/webapps/phpmyadmin/config.inc.php` and add a value for **blowfish_secret**

```text
micro /etc/webapps/phpmyadmin/config.inc.php 
```
Generated a random string
```
echo $(openssl rand -hex 16)
```

Add the value inside the empty quotation marks
```
$cfg['blowfish_secret'] = ''
```

Add a tempdir config
```
$cfg['TempDir'] = '/var/tmp'
```

Save the file and restart the `httpd` service
```text
systemctl restart httpd
```


## Test phpMyAdmin
Open your browser and navigate to
```text
http://ip.x.y.z/phpmyadmin
```
