---
published: true
date: '2019-10-01 00:00'
publish_date: '2019-10-01 00:00'
title: 'Systemd unit manual'
taxonomy:
    category:
        - docs
metadata:
    author: linux-aarhus
---

Especially the **%i** is interesting in terms of user instantiated mount units

[https://www.freedesktop.org/software/systemd/man/systemd.unit.html](https://www.freedesktop.org/software/systemd/man/systemd.unit.html)

And another idea

* [https://askubuntu.com/questions/1021643/how-to-specify-a-password-when-mounting-a-smb-share-with-gio](https://askubuntu.com/questions/1021643/how-to-specify-a-password-when-mounting-a-smb-share-with-gio)
* [https://unix.stackexchange.com/questions/296148/how-to-remount-a-systemd-mount-unit-with-different-options](https://unix.stackexchange.com/questions/296148/how-to-remount-a-systemd-mount-unit-with-different-options)
