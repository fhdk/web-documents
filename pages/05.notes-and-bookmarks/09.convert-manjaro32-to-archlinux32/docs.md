---
title: 'Convert Manjaro32 to Archlinux32'
taxonomy:
    category:
        - docs
media_order: mjro32-to-arch32.png
published: true
date: '2020-09-03 16:14'
publish_date: '2020-09-03 16:14'
---

## These are the rough notes - tested using a VirtualBox VM

![](mjro32-to-arch32.png)

**The notes are valid as of September 3. 2020**

---
## Mogrifying Manjaro32 18.0.4 to Archlinux32

Get a new mirrorlist

[https://git.archlinux32.org/packages/plain/core/pacman-mirrorlist/mirrorlist][2]

edit pacman.conf
- remove  manjaro-system from HoldPkg line
- remove SyncFirst line (manjaro patch)

Follow instructions from **Transition ...** section at [https://www.archlinux32.org/download/][3]

Accept package replacement/removal when required

```
sudo pacman -Syy archlinux32-keyring-transition
sudo pacman -S archlinux32-keyring
```

Remove conflicting packages

```
sudo pacman -Rdd pacman-mirrors manjaro-system manjaro-release bashrc-manjaro libxxf86misc menulibre
```

Install Arch pacman and pacman-contrib

```
sudo pacman -S pacman pacman-contrib
```

Remove

```
sudo pacman -Rns manjaro-settings-manager manjaro-settings-manager-notifier
```

Install - fix two conflicts

```
sudo pacman -Syu base linux linux-headers filesystem grub sudo systemd systemd-libs systemd-sysvcompat xorg-server xorg-server-common xorg-server-xwayland --overwrite --overwrite '/usr/include/crypt.h' --overwrite '/usr/lib/libcrypt.so'
```

build initramfs

    sudo mkinitcpio -p linux

remove manjaro kernel (remove any dependencies - like drivers - remember to install opensource drivers if necessary)

    sudo pacman -Rns linux414

edit `/etc/default/grub` and set `GRUB_DISTRIBUTOR` to Arch32 then rebuild grub configuration

    sudo grub-mkconfig -o /boot/grub/grub.cfg

Update

    sudo pacman -Syu

You now have a Manjaro themed Archlinux32

[1]: osdn.net/projects/manjaro32
[2]: https://git.archlinux32.org/packages/plain/core/pacman-mirrorlist/mirrorlist
[3]: https://www.archlinux32.org/download/