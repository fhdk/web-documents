---
title: 'Manjaro Secure Boot'
taxonomy:
    category:
        - docs
---

# Secure boot

Credit to [@openminded][1] at the Manjaro forum.

Without this [writeup][2] it would have taken considerably longer to implement.

I am very grateful you took the time to do this writeup.

The pages now following is my notes while applying this knowledge to a one of my laptops.



[1]: https://forum.manjaro.org/u/openminded
[2]: https://forum.manjaro.org/t/howto-use-tpm2-to-unlock-luks-partition-on-boot/101626