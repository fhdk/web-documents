---
title: 'Verify luks  setup'
taxonomy:
    category:
        - docs
---

> At this stage you might want to check your cryptsetup status:


```
cryptsetup luksDump /dev/nvme0n1p2
```

> Normally you should have 1 or 2 keyslots already occupied, the first one is your passphrase, the other is probably /crypto_keyfile.bin, and no tokens in place (yet). I suggest nuking your slot 1 if it’s occupied by crypto_keyfile at the moment (since it’s not safe to use crypto_keyfile in configuration we’re building):

As of 2022-02-07 a pristine installation only occupies Keyslot [0] - I have to watch carefully if I experiment with other ciphers than aes-xts-plaini64 and the following command would have no effect on this system

```
cryptsetup luksKillSlot /dev/nvme0n1p2 1
```

> Double-check, of course, I doubt you want to destroy a slot with actual passphrase.