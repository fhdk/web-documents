---
title: 'initrd configuration'
taxonomy:
    category:
        - docs
---

It’s time to prepare initrd part of your boot image. 

You need to decide which path you prefer: udev-based initrd or systemd-based one. 

If you have no idea, choose the first (Manjaro/Arch default). Hereinafter everything hallmarked with (a) should be considered referring to udev-based initrd setup, and everything with a foregoing (b) literal – referring to systemd-based setup.

Backup your existing mkinitcpio file with a timestamp

```

# cp /etc/mkinitcpio.conf /etc/mkinitcpio.conf.$(date +%Y%m&d%H%M)
```

Then edit your `/etc/mkinitcpio.conf` and replace the line HOOKS=

__Either the udev type (a)__

```

HOOKS=(base udev keyboard consolefont autodetect plymouth modconf block plymouth-tpm2-totp tpm2 plymouth-encrypt lvm2 resume filesystems)
```

__Or the systemd type (b)__

```

HOOKS=(base systemd keyboard sd-vconsole autodetect sd-plymouth modconf block sd-plymouth-tpm2-totp sd-encrypt lvm2 filesystems)
```

For scripting purpose - using sed to edit the file in place - adding the previous hooks for backup purpose

__Either udev type (a)__

```

OLD_HOOKS=$(cat /etc/mkinitcpio.conf | grep "^HOOKS=(")
sed -i 's/"${OLD_HOOKS}"/HOOKS=(base udev keyboard consolefont autodetect plymouth modconf block plymouth-tpm2-totp tpm2 plymouth-encrypt lvm2 resume filesystems)/g' /etc/mkinitcpio.conf 
echo "#previous_$OLD_HOOKS" | tee -a /etc/mkinitcpio.conf
```

__Or systemd type (b)__

```

OLD_HOOKS=$(cat /etc/mkinitcpio.conf | grep "^HOOKS=(")
sed -i 's/'"${OLD_HOOKS}"'/HOOKS=(base systemd keyboard sd-vconsole autodetect sd-plymouth modconf block sd-plymouth-tpm2-totp sd-encrypt lvm2 filesystems)/g' /etc/mkinitcpio.conf 
echo "#previous_$OLD_HOOKS" | tee -a /etc/mkinitcpio.conf
```

Add an entry about your LUKS partition to crypttab (discard is optional)
```

CRYPT_PART=/dev/nvme0n1p2
echo cryptlvm $(blkid -s UUID -o value $CRYPT_PART) - tpm2-device=/dev/tpmrm0,tpm2-pcrs=0+7+14,nofail,discard,x-initrd.attach | tee -a /etc/crypttab.initramfs
```

For better (de)compression speed/efficiency ratio consider setting __COMPRESSION="zstd"__ in your `/etc/mkinitcpio.conf` if not set already.  
__Note__ This works only with kernels from 5.10 and onwards

```

sed -i 's/#COMPRESSION="zstd'/COMPRESSION="zstd"/g' /etc/mkinitcpio.conf
```     

This is __not__ a good time to loose power or ... dang - I have to start over ....


kernel 5.10/5.15 systemd type partial script
```

CRYPT_PART=/dev/nvme0n1p2
echo cryptlvm $(blkid -s UUID -o value $CRYPT_PART) - tpm2-device=/dev/tpmrm0,tpm2-pcrs=0+7+14,nofail,discard,x-initrd.attach | tee -a /etc/crypttab.initramfs
cp /etc/mkinitcpio.conf /etc/mkinitcpio.conf.$(date +%Y%m&d%H%M)
OLD_HOOKS=$(cat /etc/mkinitcpio.conf | grep "^HOOKS=(")
sed -i 's/'"${OLD_HOOKS}"'/HOOKS=(base systemd keyboard sd-vconsole autodetect sd-plymouth modconf block sd-plymouth-tpm2-totp sd-encrypt lvm2 filesystems)/g' /etc/mkinitcpio.conf 
echo "#previous_$OLD_HOOKS" | tee -a /etc/mkinitcpio.conf
sed -i 's/#COMPRESSION="zstd'/COMPRESSION="zstd"/g' /etc/mkinitcpio.conf
```