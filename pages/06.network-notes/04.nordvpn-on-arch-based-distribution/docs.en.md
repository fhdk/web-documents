---
title: 'NordVPN on Arch based distribution'
taxonomy:
    category:
        - docs
---

Cross posted @ [Manjaro Forum][1]

**Difficulty: ★☆☆☆☆**

## NordVPN on Manjaro
From my notepad https://root.nix.dk/en/network-notes/nordvpn-on-arch-based-distribution

### What this topic is not
This is not a topic on installing NordVPN

### But what is it
It is a topic on setting up Manjaro to ensure the nameserver settings is restored on disconnecting NordVPN and a topic describing how I did the troubleshooting.

### Assumptions and prerequisite
* You have installed nordvpn using the custom package **nordvpn-bin**
* You have added yourself to the group **nordvpn** 
  - verify using the **groups** command
* You have enabled and started the service **nordvpnd**
* You have logged in to nordvpn using the command **nordvpn login**
   - You have followed the instructions provided by the command

### Let's get to it
I have subscription with a couple of VPN providers

* NordVPN
* ProtonVPN

I haven't been using NordVPN much because I had a DNS issue I didn't care to troubleshoot because I also have ProtonVPN - but I got an offer I couldn't refuse - so revisited NordVPN - and damn - the DNS issue was still present.

I took a long chat with NordVPN - first the chat-bot - then a human - which - due the complete lack of logfile - repeatedly told me Arch is not supported and they were not able to assist me.

### Reproduce the issue

1. List the content of your `/etc/resolv.conf`
   ```
   cat /etc/resolv.conf
   ```
2. Connect to NordVPN
   ```
   nordvpn c
   ```
3. List the content of resolv.conf again
   ```
   cat /etc/resolv.conf
   ```
4. Disconnect from NordVPN
   ```
   nordvpn d
   ```
5. List your resolv.conf for the last time

One would expect the resolv.conf to be restored - but it is not - it is still containing the NordVPN nameservers.

I not very good at being rejected with a seemingly legit case - so easy to reproduce suffice you are using NordVPN - but I also realize that NordVPN probably would have fixed if it was an issue - so time to do some digging.

## Troubleshooting NordVPN on Manjaro
I inspected the pamac build folder and unpacked the content of the deb file but I was not able to draw any conclusions - everything appeared to be as it should for a systemd installation.

### Snooping around in a Xubuntu VM
There is only one way to troubleshoot - and it is live - using Ubuntu - in a virtual machine.

I downloaded the latest Xubuntu 21.04 and fired up virtual machine using - mostly - the defaults - I usually up the VRAM to 4G and the VCPU to 2 and created a minimal installation and restarted the vm.

Before I did anything I inspected the `/etc/resolv.conf` and that was a give-away - Ubuntu uses systemd-resolved where I know Manjaro uses resolvconf from the package openresolv.

Then I downloaded the same deb file which is used by the `nordvpn-bin` package in AUR and installed it using dpkg

    sudo dpkg -i nordvpn_3.10.0-1_amd64.deb

Then I did the same tests as above - and NordVPN works flawless. Now that I got hinted to `systemd-resolved` the rest was a piece of :cheese::cake:

## Issue summary
Manjaro uses Network Manager and openresolv to set dns servers.

```
➜  ~ cat /etc/resolv.conf
# Generated by NetworkManager <----------- Note this line
search net.nix.dk
nameserver 192.168.1.5
```
Launch nordvpn using the command `nordvpn c`

```
➜  ~ cat /etc/resolv.conf
# Generated by resolvconf      <----------- Note this line
nameserver 103.86.96.100
nameserver 103.86.99.100
```

The system settings in `/etc/resolv.conf` is not restored on command `nordvpn disconnect` but remains using nordvpn dns.

And note the `<--------` above - indicating that NordVPN client is using resolvconf to rewrite the nameservers.

## Setup Manjaro to use NordVPN

Remove openresolv or resolvconf - whichever is installed
```
sudo pacman -R openresolv
```

Install the package **systemd-resolvconf** which is a helper package for interaction by apps depending on resolvconf
```
sudo pacman -Syu systemd-resolvconf
```

Enable systemd-resolved
```
sudo systemctl enable --now systemd-resolved
```

Backup the existing `resolv.conf`
```
sudo mv /etc/resolv.conf /etc/resolv.conf.bak
```

Create a symlink to `stub-resolv.conf`
```
sudo ln -s /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
```

### Before connect to NordVPN
Show dns server in use
```
➜  ~ resolvectl status
Global
           Protocols: +LLMNR +mDNS -DNSOverTLS DNSSEC=no/unsupported
    resolv.conf mode: stub
  Current DNS Server: 192.168.1.5
         DNS Servers: 192.168.1.5
Fallback DNS Servers: 1.1.1.1 9.9.9.10 8.8.8.8 2606:4700:4700::1111 2620:fe::10
                      2001:4860:4860::8888
          DNS Domain: net.nix.dk

Link 2 (eno1)
    Current Scopes: DNS LLMNR/IPv4
         Protocols: +DefaultRoute +LLMNR -mDNS -DNSOverTLS DNSSEC=no/unsupported
Current DNS Server: 192.168.1.5
       DNS Servers: 192.168.1.5
```
### Connect to NordVPN
```
nordvpn c
```
Again show dns server(s) in use
```
➜  ~ resolvectl status
Global
           Protocols: +LLMNR +mDNS -DNSOverTLS DNSSEC=no/unsupported
    resolv.conf mode: stub
  Current DNS Server: 192.168.1.5
         DNS Servers: 192.168.1.5
Fallback DNS Servers: 1.1.1.1 9.9.9.10 8.8.8.8 2606:4700:4700::1111 2620:fe::10
                      2001:4860:4860::8888
          DNS Domain: net.nix.dk

Link 2 (eno1)
    Current Scopes: DNS LLMNR/IPv4
         Protocols: +DefaultRoute +LLMNR -mDNS -DNSOverTLS DNSSEC=no/unsupported
Current DNS Server: 192.168.1.5
       DNS Servers: 192.168.1.5

Link 6 (tun0)
    Current Scopes: DNS LLMNR/IPv4 LLMNR/IPv6
         Protocols: +DefaultRoute +LLMNR -mDNS -DNSOverTLS
                    DNSSEC=allow-downgrade/unsupported
Current DNS Server: 103.86.96.100
       DNS Servers: 103.86.96.100 103.86.99.100
        DNS Domain: ~.
```
Disconnect NordVPN
```
nordvpn d
```

### An interesting lesson. 
Note the Fallback DNS Servers in the above output. 
If you want to get rid of those it is easy 
- Create a drop-in config 
   ```
   sudo touch /etc/systemd/resolved.conf.d/fallback_dns.conf
   ```` 
- Edit the file and insert the content
   ```
   [Resolve]
   FallbackDNS=
   ```
- Then restart the resolver
   ```
   sudo systemctl restart systemd-resolved
   ```
- List nameservers in use
   ```
   ➜  ~ resolvectl status
   Global
          Protocols: +LLMNR +mDNS -DNSOverTLS DNSSEC=no/unsupported
   resolv.conf mode: stub
   
   Link 2 (eno1)
      Current Scopes: DNS LLMNR/IPv4
        Protocols: +DefaultRoute +LLMNR -mDNS -DNSOverTLS DNSSEC=no/unsupported
      DNS Servers: 192.168.1.5
       DNS Domain: net.nix.dk
   ```

## Conclusion
Now armed with the knowledge on how to get NordVPN running as expected I took another chat with NordVPN and provided - in short - the solution to the issue and urged the chat guy to forward - to the people in charge of their knowledgebase - the link to the solution. So if the future you should spot a KB article on NordVPN on this issue - you know where it came from. :slight_smile: 

I cannot possible know each and every VPN provider but don't underestimate the value of knowing this little piece of info.

Who knows - maybe switching your installation from using openresolv to using systemd-resolved - is just that trick that solve your VPN issue?

In the article on [Arch Wiki - systemd-resolved][1] is another tip for the case VPN or DHCP clients expects a working resolvconf binary - it was not necessary to add - but it is good to know it is there.

## [date=2023-09-12 timezone="Atlantic/Jan_Mayen"]

If you get socket error 

    sudo systemctl enable --now nordvpnd.socket

[1]: https://wiki.archlinux.org/title/Systemd-resolved
